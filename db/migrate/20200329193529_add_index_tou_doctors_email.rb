class AddIndexTouDoctorsEmail < ActiveRecord::Migration[6.0]
  def change
    add_index :doctors, :email, unique: true
  end
end
