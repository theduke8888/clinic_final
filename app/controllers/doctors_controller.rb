class DoctorsController < ApplicationController
	before_action :check_logged_in
	before_action :correct_user, only: [:update, :edit], unless: -> {current_user.nil?}
	before_action :correct_doctor, only: [:update, :edit], unless: -> {current_doctor.nil?}

	def index
		#@doctors = Doctor.all
		@doctors = Doctor.order(:name).page(params[:page]).per(15)
	end

	def show
		@doctor = Doctor.find(params[:id])
	end

	def new
		@doctor = Doctor.new

	end

	def create
		@doctor = Doctor.new(doctor_params)
		if @doctor.save
			flash[:success] = "New Doctor account created"
			redirect_to doctors_url
		else
			render 'new'
		end

	end

	def edit
		@doctor = Doctor.find(params[:id])
	end

	def update
		@field = Doctor.find(params[:id])
		if @field.update_attributes(doctor_params)
			flash[:success] = "Doctor Updated"
			redirect_to doctors_url
		else
			render 'edit'
		end
	end

	def destroy
		Doctor.find(params[:id]).delete
		flash[:success] = "Doctor Deleted"
		redirect_to doctors_url
	end

	private
		def doctor_params
			params.require(:doctor).permit(:name, :license, :contact, :address, :email, :password, :password_confirmation, :field_id)
		end


end
